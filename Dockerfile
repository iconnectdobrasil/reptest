FROM openjdk

WORKDIR /app
COPY Main.java /app/Main.java
CMD ["java", "/app/Main.java"]
